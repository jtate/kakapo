# Kakapo

![The Kakapo](images/the_kakapo.jpg)

## What's It Do?

So far:

- It can stop/start MyResearch proxies.
- It can report back the branches of git repositories within the directory where the command is run. 

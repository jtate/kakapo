'use strict';

var chalk = require('chalk');
var exec = require('child_process').exec;

module.exports = function (proxy, self) {

  // var proxies = [
  //   'squirrel',
  //   'pws_authenticated_user',
  //   'scio_identified_records',
  //   'scio_unidentified_records',
  //   'identity_search'
  // ];

  var stopStart = function (proxy) {
    return [
      'sudo stop nodejs_ws_proxy_' + proxy,
      'sudo start nodejs_ws_proxy_' + proxy
    ];
  };

  stopStart(proxy).forEach(function(command) {
    exec(command, function(error, stdout, stderr) {
      self.log(chalk.blue(stdout));
      if (stderr) {
        self.log(chalk.red('stderr: ' + stderr));
      }
      if (error !== null) {
          self.log(chalk.red('exec error: ' + error));
      }
    });
  });

};

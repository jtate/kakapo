'use strict';

var fs = require('fs');
var nodegit = require('nodegit');
var chalk = require('chalk');
var path = require('path');

module.exports = function (pathToCheck, self) {

  if (!pathToCheck) {
    pathToCheck = '.';
  }

  if (fs.lstatSync(pathToCheck).isDirectory()) {
    var dirs = fs.readdirSync(pathToCheck);
    dirs.forEach(function(dir){
      var pathToUse = path.resolve(dir);
      nodegit.Repository.open(pathToUse)
        .then(function (repo) {
          repo.getCurrentBranch().then(function(ref){
            var branchName = ref.shorthand();
            if (branchName !== 'develop') {
              self.log(dir, chalk.grey('is on'), chalk.yellow(branchName));
            } else {
              self.log(dir, chalk.grey('is on'), chalk.green(branchName));
            }
          });
        });
    });
  } else {
    self.log(chalk.red('not a directory'));
  }

};

#! /usr/bin/env node

'use strict';

var vorpal = require('vorpal')();

var checkBranches = require('./lib/check-branches');
var restartProxy = require('./lib/restart-proxy');

vorpal
  .command('restart <proxy>')
  .description('Stops and starts a MyResearch proxy')
  .autocompletion(function(text, iteration, callback) {
    var proxies = [
      'squirrel',
      'pws_authenticated_user',
      'scio_identified_records',
      'scio_unidentified_records',
      'identity_search'
    ];
    if (iteration > 1) {
      callback(void 0, proxies);
    } else {
      var match = this.match(text, proxies);
      if (match) {
        callback(void 0, proxies);
      } else {
        callback(void 0, void 0);
      }
    }
  })
  .action(function(args, callback) {
    const self = this;
    var proxy = args.proxy;
    restartProxy(proxy, self);
    callback();
  });

vorpal
  .command('branches [path]')
  .description('Checks and reports the branch names of sibling git repos.')
  .action(function(args, callback) {
    const self = this;
    var path = args.path;
    checkBranches(path, self);
    callback();
  });


  vorpal
    .delimiter('kakapo ❯ ')
    .show();

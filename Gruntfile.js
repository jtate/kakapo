'use strict';
module.exports = function (grunt) {

  require('time-grunt')(grunt);
  require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

  grunt.initConfig({
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'tests/test.css': 'tests/test.scss'
        }
      }
    }
  });

  grunt.registerTask('default', ['sass']);

};
